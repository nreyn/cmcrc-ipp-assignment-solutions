import csv
import pprint
from datetime import datetime, timedelta
__author__ = "### YOUR NAME HERE ###"
__maintainer__ = "Nick Reynolds"
__email__ = "nick.reynolds@mq.edu.au"


# PART 1: Loading and Parsing a File

def read_stock_file(filename):
    """
    Given a filename, read the csv file into a list of dictionaries
    e.g. the filename variable above could be stocks.csv
    so you will want to read that in using the csv.DictReader.
    """
    results = []
    with open(filename) as f:
        reader = csv.DictReader(f)
        for line in reader:
            results.append(line)
    return results


def parse_stocks(stocks):
    """
    Stocks is going to be a list of stocks, read in from the above read_stock_file function.
    In this function we want to correct the data types of what we have read in.
    You will notice that the values you have read in are all strings,
    we want to convert:
        * 'Units Available' to integer
        * 'price' to float/decimal
        * (advanced) 'date' to python date
    """
    for stock in stocks:
        stock['Units Available'] = int(stock['Units Available'])
        stock['Price'] = float(stock['Price'].replace('$', ''))
        stock['Date'] = datetime.strptime(stock['Date'], "%Y-%m-%d")


# PART 2: Modifying a class

class Buffett:
    money = 0
    purchases = None

    def __init__(self, money):
        """ Don't change this function """
        self.money = money
        self.purchases = {}

    def buy(self, company, quantity, price):
        """
        This function handles Buffett buying a stock.
        It needs to:
            * Check if they have enough money to buy the quantity at that price (quantity * price)
            * Decrease money if the purchase is going through
            * Add the company and quantity to the purchases (self.purchases) dictionary.

        The purchases dictionary keeps track of his purchases with the format
        {
            companyA: quantity,
            companyB: quantity
        }
        """
        # pass
        cost = quantity * price
        if cost <= self.money:
            if company not in self.purchases:
                self.purchases[company] = 0
            self.purchases[company] += quantity
            self.money -= quantity * price

    def how_many_can_i_sell(self, company, potential_purchase_quantity):
        """
        Determine how many of a stock buffett can sell, i.e. check the purchases for that company.
        We want to return the maximum amount of stock we can buy, so either we return the number of stock
        we have for that company, or the potential purchase quantity, whichever is lower.
        """
        if company in self.purchases:
            return min(self.purchases[company], potential_purchase_quantity)
        return 0

    def sell(self, company, quantity, price):
        """
        Sell the stock if we can. Check how many of the stock we have (see above).
        This function needs to
         * Change the quantity in the self.purchases dictionary
         * Increate our money by quantity * price
        """
        how_many = self.how_many_can_i_sell(company, quantity)
        if how_many > 0:
            self.purchases[company] -= how_many
            self.money += how_many * price


# PART 3: Conditional rules


def open_market(money, stocks):
    """
    Here we are going to process our stocks according to the rules in the assignment specification.
    Again stocks is your list of stocks. Money is the amount of money you start with.
    Rules:
     * For AAA
        * Buy whenever the price is <= $20
        * Sell whenever the price is >= $30
     * For BBB
        * Buy whenever the price is <= $50
        * Sell whenever the price is >= $90
     * For CCC (Harder)
        * Buy whenever the price is <= the minimum of the previous day
        * Sell whenever the price is >= the maximum of the previous day
     * For DDD (Harder)
        * Buy whenever the price is <= the average price on the previous day
        * Sell whenever the price is >= the maximum of the previous day

    You *Must* use the Buffett class defined above. Code has been left below for demo purposes.
    """
    warren = Buffett(money)
    # Making a dict to store aggregates
    aggregates = {}
    day_stocks = []
    previous_date_loop = None
    for stock in stocks:
        company = stock['Company']
        price = stock['Price']
        quantity = stock['Units Available']
        date = stock['Date']
        previous_date = date - timedelta(1)
        print(date, previous_date)
        if company == 'AAA':
            if price <= 20:
                warren.buy(company, quantity, price)
            if price >= 30:
                warren.sell(company, quantity, price)
        elif company == 'BBB':
            if price <= 50:
                warren.buy(company, quantity, price)
            if price >= 90:
                warren.sell(company, quantity, price)
        elif company == 'CCC':
            # Get the min and max out of the aggregate if one exists for this day/company
            if previous_date in aggregates and company in aggregates[previous_date]:
                    previous_min = aggregates[previous_date][company]['min']
                    previous_max = aggregates[previous_date][company]['max']
                    if price <= previous_min:
                        warren.buy(company, quantity, price)
                    elif price >= previous_max:
                        warren.sell(company, quantity, price)
        elif company == 'DDD':
            # Get the min and max out of the aggregate if one exists for this day/company
            if previous_date in aggregates and company in aggregates[previous_date]:
                    previous_avg = aggregates[previous_date][company]['avg']
                    previous_max = aggregates[previous_date][company]['max']
                    if price <= previous_avg:
                        warren.buy(company, quantity, price)
                    elif price >= previous_max:
                        warren.sell(company, quantity, price)

        if previous_date_loop != date:  # We have finished getting stocks for the day, compute aggregates
            aggregates[date] = compute_aggregates(day_stocks)
            previous_date_loop = date
            day_stocks = []
        day_stocks.append(stock)

    return warren


def compute_aggregates(stocks):
    """
    A general function that will compute aggregates for each companies stocks.
    We will end up with aggregate figures like the below

    'AAA': {   'avg': 49.24896551724137,
                'count': 58,
                'max': 95.95,
                'min': 0.65,
                'total': 2856.4399999999996},
    'BBB': {   'avg': 53.525789473684206,
                'count': 76,
                'max': 99.13,
                'min': 1.29,
                'total': 4067.9599999999996},

    """
    companies = {}
    for stock in stocks:
        company = stock['Company']
        price = stock['Price']
        if company not in companies:
            companies[company] = {
                'max': price,
                'min': price,
                'count': 1,
                'total': price,
                'avg': price
            }
        else:
            companies[company]['count'] += 1
            companies[company]['total'] += price
            if price < companies[company]['min']:
                companies[company]['min'] = price
            if price > companies[company]['max']:
                companies[company]['max'] = price
            companies[company]['avg'] = companies[company]['total'] / companies[company]['count']
    return companies


if __name__ == '__main__':
    s = read_stock_file('stocks.csv')
    print('Stocks!', s)
    parse_stocks(s)
    result = open_market(2000, s)
    print(result.money)
    print(result.purchases)